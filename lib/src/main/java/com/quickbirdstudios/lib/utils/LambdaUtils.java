package com.quickbirdstudios.lib.utils;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.Toast;

/**
 * Created by sebastiansellmair on 23.01.18.
 */

public class LambdaUtils {

    public static void tryOrToast(@NonNull Context context, @NonNull ExceptionRunnable action) {
        try {
            action.run();
        } catch (Exception e) {
            Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
        }
    }

    public static void tryOrToast(@NonNull Context context,
                                  @NonNull String message,
                                  @NonNull ExceptionRunnable action) {
        try {
            action.run();
        } catch (Exception e) {
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        }
    }

    @Nullable
    public static <T> T tryOrNull(@NonNull ReturnRunnable<T> returnRunnable) {
        try {
            return returnRunnable.run();
        } catch (Throwable throwable) {
            return null;
        }
    }


    /*
  Helper interface
   */
    public interface ExceptionRunnable {
        void run() throws Exception;
    }


    public interface ArgumentExceptionRunnable<T> {
        void run(T t) throws Exception;
    }

    public interface ReturnRunnable<T> {
        T run() throws Throwable;
    }
}
