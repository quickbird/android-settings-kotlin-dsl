package com.quickbirdstudios.lib.settings;

import android.support.annotation.Nullable;

/**
 * Created by sebastiansellmair on 05.02.18.
 */

interface SettingSave {
    interface String {
        void onSave(java.lang.String value);
    }

    interface Double {
        void onSave(double value);
    }

    interface Int {
        void onSave(int value);
    }

    interface Float {
        void onSave(float value);
    }

    interface Boolean {
        void onSave(boolean value);
    }

    interface Point {
        void onSave(@Nullable android.graphics.Point point);
    }
}
