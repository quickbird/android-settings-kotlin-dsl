package com.quickbirdstudios.lib.settings;

import android.support.v7.widget.RecyclerView;

import java.lang.ref.WeakReference;

/**
 * Created by sebastiansellmair on 05.02.18.
 */

public abstract class AbstractSettingsItem {

    private WeakReference<SettingsViewHolder> viewHolderReference
            = new WeakReference<>(null);

    final void invalidate() {
        SettingsViewHolder viewHolder = viewHolderReference.get();
        if (viewHolder != null) {
            bind(viewHolder);
        }
    }

    final void bind(SettingsViewHolder viewHolder) {
        viewHolder.setMaster(this);
        this.viewHolderReference = new WeakReference<>(viewHolder);
        this.onBind(viewHolder);
    }

    abstract void onBind(SettingsViewHolder viewHolder);

    final void unbind(RecyclerView.ViewHolder holder) {
        this.viewHolderReference = new WeakReference<>(null);
        this.onUnbind(holder);
    }

    void onUnbind(RecyclerView.ViewHolder holder) {

    }


    void save() {

    }
}
