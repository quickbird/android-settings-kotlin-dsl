package com.quickbirdstudios.lib.settings;

/**
 * Created by sebastiansellmair on 05.02.18.
 */

public class SettingsToggleItem extends AbstractSettingsItem {

    String label;
    boolean state;
    SettingSave.Boolean saver;

    @Override
    void onBind(SettingsViewHolder viewHolder) {
        if (!(viewHolder instanceof SettingsToggleItemViewHolder)) return;
        SettingsToggleItemViewHolder vh = (SettingsToggleItemViewHolder) viewHolder;

        vh.label.setText(label);
        vh.toggle.setChecked(state);

        vh.toggle.setOnCheckedChangeListener((v, checked) -> this.state = checked);
    }

    @Override
    void save() {
        if (saver != null) {
            saver.onSave(state);
        }
    }
}
