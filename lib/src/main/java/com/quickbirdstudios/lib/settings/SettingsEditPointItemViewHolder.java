package com.quickbirdstudios.lib.settings;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.quickbirdstudios.lib.R;
/**
 * Created by sebastiansellmair on 05.02.18.
 */

class SettingsEditPointItemViewHolder extends SettingsViewHolder {

    final TextView label;
    final EditText editX;
    final EditText editY;

    private SettingsEditPointItemViewHolder(View itemView) {
        super(itemView);
        label = itemView.findViewById(R.id.label);
        editX = itemView.findViewById(R.id.editX);
        editY = itemView.findViewById(R.id.editY);
    }

    static SettingsEditPointItemViewHolder newInstance(ViewGroup parent) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.item_setting_edit_point,
                parent,
                false);

        return new SettingsEditPointItemViewHolder(view);
    }


}
