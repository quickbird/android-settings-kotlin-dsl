package com.quickbirdstudios.lib.settings;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Created by sebastiansellmair on 28.02.18.
 */

public class SettingsLabelItem extends AbstractSettingsItem {

    @Nullable
    String label = null;

    @Nullable
    String value = null;

    @NonNull
    Size valueSize = Size.MEDIUM;

    @Override
    void onBind(SettingsViewHolder viewHolder) {
        if (!(viewHolder instanceof SettingsLabelViewHolder)) return;
        SettingsLabelViewHolder vh = (SettingsLabelViewHolder) viewHolder;
        vh.label.setText(label);
        vh.value.setText(value);

        vh.value.setTextSize(valueSize.textSize);
    }

    enum Size {
        SMALL(12),
        MEDIUM(14),
        LARGE(16);

        private int textSize;

        Size(int textSize) {
            this.textSize = textSize;
        }
    }
}
