package com.quickbirdstudios.lib.settings;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.widget.EditText;

/**
 * Created by sebastiansellmair on 05.02.18.
 */

public class SettingsEditItem extends AbstractSettingsItem {

    @Nullable
    String label;

    @Nullable
    String value;

    @NonNull
    SettingType type = SettingType.TEXT;

    @Nullable
    SettingSave.String settingSave;

    private TextListener textListener = new TextListener(string -> value = string);


    @Override
    void onBind(SettingsViewHolder viewHolder) {
        if (!(viewHolder instanceof SettingsEditItemViewHolder)) return;
        SettingsEditItemViewHolder vh = (SettingsEditItemViewHolder) viewHolder;

        vh.label.setText(label);
        vh.edit.setText(value);

        vh.edit.addTextChangedListener(textListener);

        applyType(vh.edit);
    }

    @Override
    void onUnbind(RecyclerView.ViewHolder holder) {
        super.onUnbind(holder);
        if (!(holder instanceof SettingsEditItemViewHolder)) return;
        ((SettingsEditItemViewHolder) holder).edit.removeTextChangedListener(textListener);
    }

    @Override
    void save() {
        if (settingSave != null) {
            settingSave.onSave(value);
        }
    }

    private void applyType(EditText edit) {
        switch (type) {
            case TEXT:
                edit.setInputType(InputType.TYPE_CLASS_TEXT);
                break;

            case INTEGER:
                edit.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_SIGNED);
                break;

            case FLOAT:
            case DOUBLE:
                edit.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
                break;

            case READ_ONLY:
                edit.setInputType(InputType.TYPE_CLASS_TEXT);
                edit.setEnabled(false);
                break;

        }

        if (type != SettingType.READ_ONLY) {
            edit.setEnabled(true);
        }
    }


}
