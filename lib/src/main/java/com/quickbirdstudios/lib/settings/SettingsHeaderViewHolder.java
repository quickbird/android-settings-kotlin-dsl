package com.quickbirdstudios.lib.settings;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.quickbirdstudios.lib.R;
/**
 * Created by sebastiansellmair on 05.02.18.
 */

class SettingsHeaderViewHolder extends SettingsViewHolder {

    final TextView label;

    private SettingsHeaderViewHolder(View itemView) {
        super(itemView);
        label = itemView.findViewById(R.id.label);
    }

    static SettingsViewHolder newInstance(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_setting_header, parent, false);

        return new SettingsHeaderViewHolder(view);
    }
}
