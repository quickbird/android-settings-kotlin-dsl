package com.quickbirdstudios.lib.settings;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

/**
 * Created by sebastiansellmair on 05.02.18.
 */

public class SettingsTitleItem extends AbstractSettingsItem {

    @Override
    void onBind(SettingsViewHolder viewHolder) {
        Context context = viewHolder.itemView.getContext();
        if (viewHolder instanceof SettingsTitleItemViewHolder) {
            ((SettingsTitleItemViewHolder) viewHolder)
                    .label
                    .setText(getVersionInfo(context));
        }
    }

    private String getVersionInfo(Context context) {
        PackageInfo packageInfo = null;
        try {
            packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
        } catch (final PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        if (packageInfo != null) {
            return "APK version: " + packageInfo.versionName + " (" + packageInfo.versionCode + ")";
        }
        return "";
    }

}
