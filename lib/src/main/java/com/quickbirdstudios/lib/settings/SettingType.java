package com.quickbirdstudios.lib.settings;

/**
 * Created by sebastiansellmair on 05.02.18.
 */

public enum SettingType {
    INTEGER,
    FLOAT,
    DOUBLE,
    TEXT,
    READ_ONLY
}
